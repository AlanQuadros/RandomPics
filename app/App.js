import React from "react";
import { Provider } from 'react-redux';
import createMainStore from './reducers/Store';
import Main from "./Main";
import { AsyncStorage } from "react-native";

const store = createMainStore();

export default class App extends React.Component {

    async componentWillMount() {
        try {
            const logado = await AsyncStorage.getItem('LOGGED');
            if (logado === 'true') {
                store.dispatch({
                    type: "USER_LOGGED",
                });
            }
        }
        catch (e) {
            alert('Error\n' + e);
        }
    }

    render() {
        return (
            <Provider store={store}>
                <Main />
            </Provider>
        );
    }
}
