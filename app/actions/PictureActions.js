import { throttle } from "lodash";
import {PICTURES} from "../resources/Urls";

const getAllPicturesByEmail = (email) => (dispatch) => {

    return fetch(PICTURES + email)
        .then(response => response.json())
        .then(responseJson => {
            dispatch({
                type: 'PICTURES_REQUEST',
                pictures: responseJson
            });
            return responseJson;
        })
        .catch((error) => {
            dispatch({
                type: "PICTURES_REQUEST_ERROR",
                errorMessage: error.message
            });
        });
};

const sendPicture = (url, city, country) => (dispatch) => {
    return fetch(PICTURES, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(
            {
                picture: url,
                city: city,
                country: country
            }
        )
    }).then((response) => response.json())
        .then((responseJson) => {
            if(!responseJson.error){
                dispatch({
                    type: "PICTURE_SEND"
                });
            }

            return responseJson;
        })
        .catch((error) => {
            dispatch({
                type: "PICTURE_SEND_ERROR",
                errorMessage: error.message
            });
        });
};

export const _getAllPicturesByEmail = throttle(getAllPicturesByEmail, 5000);
export const _sendPicture = throttle(sendPicture, 5000);