export const colors = {
    black: '#000',
    white: '#fff',
    blue: '#75a9f9',
    grey: '#aaa',
    darkBlue: '#033076'
};