const URL_BASE = 'https://randompics-api.herokuapp.com/';

export const LOGIN = URL_BASE + 'login';
export const USERS = URL_BASE + 'users';
export const PICTURES = URL_BASE + 'foto/';