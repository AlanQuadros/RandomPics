import React from 'react';
import {View, Image, StyleSheet, Text, TouchableWithoutFeedback, Keyboard, findNodeHandle} from 'react-native';
import { BlurView } from 'react-native-blur';
import { Sae } from 'react-native-textinput-effects';
import Entypo from 'react-native-vector-icons/Entypo';
import {colors} from "../resources/Colors";
import Button from "../components/Button";
import * as LoginActions from '../actions/LoginActions';
import { connect } from "react-redux";


class LoginContainer extends React.PureComponent {

    state = {
        email: '',
        password: '',
        viewRef: null
    };

    login = () => {
        Keyboard.dismiss();

        const { _doLogin } = this.props;

        _doLogin(this.state.email, this.state.password)
            .then(resp => {

            });
    };

    imageLoaded() {
        this.setState({ viewRef: findNodeHandle(this.backgroundImage) });
    };

    render() {
        return (
            <View style={ styles.container }>
                <Image
                    resizeMode={'cover'}
                    ref={(img) => { this.backgroundImage = img; }}
                    source={ require('../../assets/imgs/background.jpg') }
                    style={{ flex: 1, transform: [{ rotate: '90deg'}] }}
                    onLoadEnd={this.imageLoaded.bind(this)}
                />
                <BlurView
                    style={styles.absolute}
                    blurType='light'
                    blurAmount={10}
                    blurRadius={10}
                    downsampleFactor={3}
                    overlayColor={'rgba(255, 255, 255, .25)'}
                    viewRef={this.state.viewRef}
                />

                <View style={ styles.inputView }>
                    <Sae
                        label={'E-mail'}
                        iconClass={Entypo}
                        iconName={'email'}
                        iconColor={colors.black}
                        labelStyle={{ color: colors.black }}
                        inputStyle={{ color: colors.black }}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        onChangeText={(email) => { this.setState({email}) }}
                    />

                    <Sae
                        label={'Password'}
                        iconClass={Entypo}
                        iconName={'key'}
                        iconColor={colors.black}
                        labelStyle={{ color: colors.black }}
                        inputStyle={{ color: colors.black }}
                        autoCapitalize={'none'}
                        secureTextEntry
                        autoCorrect={false}
                        onChangeText={(password) => { this.setState({password}) }}
                    />

                    <View
                        style={{
                            marginVertical: 30
                        }}>
                        <Button
                            action={ () => this.login() }
                            label={'Sign in'}
                            width={200}/>

                        <TouchableWithoutFeedback
                            onPress={ () => this.props.navigation.navigate('Signup') }>
                            <View>
                                <Text style={ styles.signupText }>
                                    Don't have an account? Sign up here.
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imgBackground: {
        flex: 1,
        transform: [{ rotate: '90deg'}]
    },
    absolute: {
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
    },
    inputView: {
        position: 'absolute',
        left: 0,
        right: 0,
        marginHorizontal: 30
    },
    signupText: {
        fontSize: 12,
        alignSelf: 'center',
        marginTop: 15,
        color: colors.black,
        textDecorationLine: 'underline'
    }
});

const mapeador = state => {
    return {
        // currentUser: state.currentUser
    };
};
LoginContainer = connect(mapeador, LoginActions)(LoginContainer);
export default LoginContainer;