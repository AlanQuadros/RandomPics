import React from 'react';
import {View, Image, StyleSheet, Dimensions, Text, ActivityIndicator, findNodeHandle, AsyncStorage} from 'react-native';
import { BlurView } from 'react-native-blur';
import PictureCard from "../components/PictureCard";
import Carousel from 'react-native-snap-carousel';
import ActionButton from 'react-native-action-button';
import ImagePicker from 'react-native-image-picker';
import {connect} from "react-redux";
import * as PictureActions from "../actions/PictureActions";
import RenderIf from "../components/RenderIf";
import {colors} from "../resources/Colors";
import Entypo from 'react-native-vector-icons/Entypo';

const width = Dimensions.get('window').width;

class MainContainer extends React.PureComponent {

    state = {
        pictures: [],
        isLoading: true,
        viewRef: null
    };

    async componentWillMount() {
        const { _getAllPicturesByEmail } = this.props;

        const email = await AsyncStorage.getItem('EMAIL');

        _getAllPicturesByEmail(email)
            .then(resp => {

                this.setState({
                    pictures: resp,
                    isLoading: false
                })
            })
    }

    _renderItem ({item}) {
        return (
            <PictureCard item={item}/>
        );
    }

    choosePhoto = () => {

        let options = {
            title: 'Select an option',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.warn('User cancelled image picker');
            } else if (response.error) {
                console.warn('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
            } else {
                // let source = {uri: response.uri};

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.props.navigation.push('Picture', { pic: response.uri, response: response });
            }
        })
    };

    imageLoaded() {
        this.setState({ viewRef: findNodeHandle(this.backgroundImage) });
    }

    render() {
        return (
            <View style={ styles.container }>
                <RenderIf condition={this.state.isLoading}>
                    <ActivityIndicator
                        style={ styles.absolute }
                        animating={ true }
                        size='large'
                        color={colors.black} />
                </RenderIf>

                <RenderIf condition={!this.state.isLoading}>
                    <Image
                        resizeMode={'cover'}
                        ref={(img) => { this.backgroundImage = img; }}
                        source={ require('../../assets/imgs/background.jpg') }
                        style={{ flex: 1, transform: [{ rotate: '90deg'}] }}
                        onLoadEnd={this.imageLoaded.bind(this)}
                    />
                    <BlurView
                        style={styles.absolute}
                        blurType='light'
                        blurAmount={4}
                        blurRadius={4}
                        downsampleFactor={5}
                        overlayColor={'rgba(255, 255, 255, .25)'}
                        viewRef={this.state.viewRef}
                    />

                    <View style={{ position: "absolute" }}>
                        {
                            this.state.pictures !== undefined ?
                                this.state.pictures.length > 0 ?
                                    <Carousel
                                        data={this.state.pictures}
                                        layout={'stack'}
                                        layoutCardOffset={15}
                                        renderItem={this._renderItem}
                                        sliderWidth={width}
                                        itemWidth={width}
                                    /> :
                                    <Text style={ styles.errorText }>
                                        Sorry, you don't have any pics :(
                                    </Text> : null
                        }
                    </View>

                    <ActionButton
                        buttonColor={colors.blue}
                        renderIcon={() => (
                            <Entypo
                                name='camera'
                                size={25}
                                color={colors.white} />
                        )}
                        onPress={() => this.choosePhoto() }
                    />

                </RenderIf>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgBackground: {
        flex: 1,
        transform: [{ rotate: '90deg'}]
    },
    absolute: {
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
    },
    errorText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: colors.black
    }
});

const mapeador = state => {
    return {
        pictures: state.pictures
    };
};
MainContainer = connect(mapeador, PictureActions)(MainContainer);
export default MainContainer;