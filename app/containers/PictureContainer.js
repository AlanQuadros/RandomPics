import React from 'react';
import {View, Image, StyleSheet, ActivityIndicator, Alert, ScrollView} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import Button from "../components/Button";
import {colors} from "../resources/Colors";
import RenderIf from "../components/RenderIf";
import { Hoshi } from 'react-native-textinput-effects';
import {connect} from "react-redux";
import * as PictureActions from "../actions/PictureActions";

class PictureContainer extends React.PureComponent {

    state = {
        isLoading: false,
        city: '',
        country: ''
    };

    uploadFile(file) {
        return RNFetchBlob.fetch('POST', 'https://api.cloudinary.com/v1_1/gamerhousefinal/image/upload?upload_preset=f6phpdlm', {
            'Content-Type': 'multipart/form-data'
        }, [
            {
                name: 'file',
                filename: file.filename,
                data: 'data:image/jpeg;base64,' + file.data
            }
        ])
    }

    sendPhoto(response) {

        this.setState({ isLoading: true });

        const { _sendPicture } = this.props;
        const { city, country } = this.state;

        this.uploadFile(response)
            .then(response => response.json())
            .then(result => {
                if(result.public_id !== undefined) {
                    _sendPicture(result.secure_url, city, country)
                        .then(resp => {

                            Alert.alert(
                                'Thank\'s for share!',
                                'Your picture will be send to a random person!',
                                [
                                    {
                                        text: 'OK',
                                        onPress: () => {
                                            this.setState({ isLoading: false });
                                            this.props.navigation.goBack();
                                        }
                                    },
                                ],
                                { cancelable: false }
                            )
                        });
                }
                // console.warn(JSON.stringify(result.secure_url));
            })
    }

    render() {
        return (
            <View style={ styles.container }>

                <RenderIf condition={this.state.isLoading}>
                    <ActivityIndicator
                        style={ styles.absolute }
                        animating={ true }
                        size='large'
                        color={colors.black} />
                </RenderIf>

                <RenderIf condition={!this.state.isLoading}>
                    <ScrollView>
                        <Image
                            style={ styles.img }
                            source={{ uri: this.props.navigation.getParam('pic', '') }} />

                        {/*<Image*/}
                            {/*resizeMode={'cover'}*/}
                            {/*source={ require('../../assets/imgs/background.jpg') }*/}
                            {/*style={ styles.img }*/}
                            {/*/>*/}

                        <View style={{ marginVertical: 15 }}>

                            <View style={{ marginBottom: 30, marginHorizontal: 25 }}>
                                <Hoshi
                                    label={'City'}
                                    borderColor={colors.black}
                                    onChangeText={(city) => { this.setState({city}) }}
                                />

                                <Hoshi
                                    label={'Country'}
                                    borderColor={colors.black}
                                    onChangeText={(country) => { this.setState({country}) }}
                                />
                            </View>

                            <RenderIf condition={ this.state.city.length > 0 && this.state.country.length > 0 }>
                                <Button
                                    label={'Share Photo'}
                                    width={250}
                                    action={ () => this.sendPhoto(this.props.navigation.getParam('response', '')) }/>
                            </RenderIf>

                        </View>

                    </ScrollView>
                </RenderIf>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignSelf: 'center'
    },
    img: {
        width: 320,
        height: 480
    },
    absolute: {
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
    },
});

const mapeador = state => {
    return {
        // pictures: state.pictures
    };
};
PictureContainer = connect(mapeador, PictureActions)(PictureContainer);
export default PictureContainer;