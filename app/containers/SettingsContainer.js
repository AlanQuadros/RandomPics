import React from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';
import {connect} from "react-redux";
import * as LoginActions from "../actions/LoginActions";
import Button from "../components/Button";
import {colors} from "../resources/Colors";

class SettingsContainer extends React.PureComponent {

    state = {
        user: {}
    };

    async componentWillMount() {

        let email = await AsyncStorage.getItem('EMAIL');
        let name = await AsyncStorage.getItem('NAME');

        let user = {
            email: email,
            name: name
        };

        this.setState({
            user
        });
    }

    render() {
        return (
            <View style={ styles.container }>
                <View style={ styles.textView }>
                    <Text style={ styles.label }>
                        { this.state.user.name }
                    </Text>

                    <Text style={ styles.label }>
                        { this.state.user.email }
                    </Text>
                </View>

                <Button
                    action={ () => this.props.logout() }
                    label={'Logoff'}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    textView: {
        alignSelf: 'center',
        marginBottom: 25
    },
    label: {
        fontSize: 18,
        color: colors.black,
        marginVertical: 15,
        alignSelf: 'center'
    }
});

const mapeador = state => {
    return {
        user: state.LoginReducer.user
    };
};
SettingsContainer = connect(mapeador, LoginActions)(SettingsContainer);
export default SettingsContainer;