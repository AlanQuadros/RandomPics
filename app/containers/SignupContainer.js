import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { BlurView } from 'react-native-blur';
import { Sae } from 'react-native-textinput-effects';
import Entypo from 'react-native-vector-icons/Entypo';
import {colors} from "../resources/Colors";
import Button from "../components/Button";
import {connect} from "react-redux";
import * as LoginActions from "../actions/LoginActions";

class SignupContainer extends React.PureComponent {

    state = {
        name: '',
        email: '',
        password: ''
    };

    login = () => {
        // alert(this.state.name + '\n' + this.state.email + '\n' + this.state.password);

        const { _signUp } = this.props;
        const { name, email, password } = this.state;

        _signUp(name, email, password)
            .then(resp => {

            })
    };

    render() {
        return (
            <View style={ styles.container }>
                {/*<Image*/}
                    {/*resizeMode={'cover'}*/}
                    {/*source={ require('../../assets/imgs/background.jpg') }*/}
                    {/*style={{ flex: 1, transform: [{ rotate: '90deg'}] }}*/}
                {/*/>*/}
                {/*<BlurView*/}
                    {/*style={styles.absolute}*/}
                    {/*blurType='light'*/}
                    {/*blurAmount={7}*/}
                {/*/>*/}

                <View style={ styles.inputView }>
                    <Sae
                        label={'Your name'}
                        iconClass={Entypo}
                        iconName={'edit'}
                        iconColor={colors.black}
                        labelStyle={{ color: colors.black }}
                        inputStyle={{ color: colors.black }}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        onChangeText={(name) => { this.setState({name}) }}
                    />

                    <Sae
                        label={'E-mail'}
                        iconClass={Entypo}
                        iconName={'email'}
                        iconColor={colors.black}
                        labelStyle={{ color: colors.black }}
                        inputStyle={{ color: colors.black }}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        onChangeText={(email) => { this.setState({email}) }}
                    />

                    <Sae
                        label={'Password'}
                        iconClass={Entypo}
                        iconName={'key'}
                        iconColor={colors.black}
                        labelStyle={{ color: colors.black }}
                        inputStyle={{ color: colors.black }}
                        autoCapitalize={'none'}
                        secureTextEntry
                        autoCorrect={false}
                        onChangeText={(password) => { this.setState({password}) }}
                    />

                    <View
                        style={{
                            marginVertical: 30
                        }}>
                        <Button
                            action={ () => this.login() }
                            label={'Sign up'}
                            width={200}/>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imgBackground: {
        flex: 1,
        transform: [{ rotate: '90deg'}]
    },
    absolute: {
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
    },
    inputView: {
        position: 'absolute',
        left: 0,
        right: 0,
        marginHorizontal: 30
    }
});

const mapeador = state => {
    return {
        // currentUser: state.currentUser
    };
};
SignupContainer = connect(mapeador, LoginActions)(SignupContainer);
export default SignupContainer;