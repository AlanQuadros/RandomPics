import React from 'react';
import {connect} from "react-redux";
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {colors} from "./resources/Colors";

import LoginScreen from "./screens/LoginScreen";
import MainScreen from "./screens/MainScreen";
import * as LoginActions from './actions/LoginActions'
import SignupScreen from "./screens/SignupScreen";
import SettingsScreen from "./screens/SettingsScreen";
import PictureScreen from "./screens/PictureScreen";

const NavigationStack = isLogged => createStackNavigator({
    LoginScreen: {
        screen: LoginScreen
    },
    TabNavigation: {
        screen: TabNavigation
    },
    Signup: {
        screen: SignupScreen
    },
    Picture: {
        screen: PictureScreen
    }
},
{
    screenProps: isLogged,
    initialRouteName: isLogged ? 'TabNavigation' : 'LoginScreen',
    // initialRouteName: 'Picture',
});

const TabNavigation = createBottomTabNavigator({
    Pictures: MainScreen,
    Settings: SettingsScreen
},
{
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) => {
            const { routeName } = navigation.state;
            let iconName;
            if (routeName === 'Pictures') {
                iconName = `ios-images${focused ? '' : '-outline'}`;
            } else if (routeName === 'Settings') {
                iconName = `ios-options${focused ? '' : '-outline'}`;
            }

            return <Ionicons name={iconName} size={25} color={tintColor} />;
        },
    }),
    tabBarOptions: {
        activeTintColor: colors.blue,
        inactiveTintColor: colors.grey,
    },
});

TabNavigation.navigationOptions = ({ navigation }) => {
    const { routes, index } = navigation.state;
    const navigationOptions = {};

    if (routes[index].routeName === 'Pictures') {
        navigationOptions.title = 'RandomPics';
    } else if (routes[index].routeName === 'Settings') {
        navigationOptions.title = 'Settings';
    }

    return navigationOptions;
};

class Main extends React.PureComponent {

    render() {
        const Navigator = NavigationStack(this.props.isLogged);
        return (
            <Navigator screenProps={{isLogged: this.props.isLogged}} />
        );
    }
}

const mapeador = state => {
    return {
        isLogged: state.LoginReducer.isLogged
    };
};

Main = connect(mapeador, LoginActions)(Main);
export default Main;
