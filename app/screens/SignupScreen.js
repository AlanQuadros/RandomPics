import React from 'react';
import SignupContainer from "../containers/SignupContainer";
import {colors} from "../resources/Colors";

export default class SignupScreen extends React.PureComponent {
    static navigationOptions = ({navigation}) => {
        return {
            headerStyle: { backgroundColor: colors.white },
            headerTitle: 'Sign up',
            headerTintColor: colors.black,
            // headerTitleStyle: layouts.header

        }
    };

    render() {
        return (
            <SignupContainer navigation={this.props.navigation}/>
        );
    }
}