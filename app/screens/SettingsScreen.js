import React from 'react';
import SettingsContainer from "../containers/SettingsContainer";
import {colors} from "../resources/Colors";

export default class SettingsScreen extends React.PureComponent {
    static navigationOptions = () => {
        return {
            headerStyle: { backgroundColor: colors.white },
            headerTitle: 'Settings',
            headerTintColor: colors.black,
        }
    };

    render() {
        return (
            <SettingsContainer navigation={this.props.navigation}/>
        );
    }
}