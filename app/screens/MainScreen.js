import React from 'react';
import MainContainer from "../containers/MainContainer";
import {colors} from "../resources/Colors";

export default class MainScreen extends React.PureComponent {
    static navigationOptions = () => {
        return {
            headerStyle: { backgroundColor: colors.white },
            headerTitle: 'RandomPics',
            headerTintColor: colors.black,
        }
    };

    render() {
        return (
            <MainContainer navigation={this.props.navigation}/>
        );
    }
}