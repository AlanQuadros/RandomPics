import React from 'react';
import PictureContainer from "../containers/PictureContainer";
import {colors} from "../resources/Colors";

export default class PictureScreen extends React.PureComponent {
    static navigationOptions = () => {
        return {
            headerStyle: { backgroundColor: colors.white },
            headerTitle: 'Your Picture',
            headerTintColor: colors.black,
        }
    };

    render() {
        return (
            <PictureContainer navigation={this.props.navigation}/>
        );
    }
}