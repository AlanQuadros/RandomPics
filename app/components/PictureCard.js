import React from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Dimensions, Easing, Share } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ZoomImage from 'react-native-zoom-image';
import {colors} from "../resources/Colors";

const width = Dimensions.get('window').width - 50;

export default class PictureCard extends React.PureComponent {

    onClickShare(picture) {
        Share.share({
            message: `#RandomPics\n ${picture}`,
            url: picture,
            title: 'RandomPics'
        }, {
        })
    }

    render() {
        const { item } = this.props;

        return (
            <View style={ styles.container }>

                <ZoomImage
                    source={{uri: item.picture}}
                    imgStyle={styles.img}
                    style={styles.img}
                    duration={200}
                    enableScaling={false}
                    easingFunc={Easing.ease}
                />

                <View style={ styles.view }>
                    <Text
                        style={ styles.labelImg }>
                        { item.place }
                    </Text>

                    <TouchableWithoutFeedback
                        onPress={ () => this.onClickShare(item.picture) }>
                        <View>
                            <Ionicons
                                name={'md-share'}
                                size={20}
                                color={colors.darkBlue} />
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignSelf: 'center',
        width: width,
        backgroundColor: 'white',
        borderRadius: 8,
        margin: 20,
        overflow: 'hidden'
    },
    img: {
        height: 250,
        width: width,
    },
    view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 10,
        marginHorizontal: 8
    },
    labelImg: {
        fontSize: 12,
        color: colors.black,
        alignSelf: 'center'
    }
});