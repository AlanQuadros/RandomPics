import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { defaultTo } from 'lodash';

export default class RenderIf extends Component {

    isFunction(input) {
        return typeof input === 'function';
    }

    /* see https://github.com/ajwhite/render-if */
    renderIf(predicate, elemOrThunk) {
        return defaultTo(predicate, false) ? (this.isFunction(elemOrThunk) ? elemOrThunk() : elemOrThunk) : null;
    }

    render() {
        return this.renderIf(this.props.condition, this.props.children);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
