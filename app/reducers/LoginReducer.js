import { combineReducers } from 'redux';

const isLogged = (state = false, action) => {
    switch (action.type) {
        case 'USER_LOGGED':
            return true;
        case 'USER_LOGOFF':
            return false;
        case 'USER_LOGIN_ERROR':
            return false;
        default:
            return state;
    }
};

export default combineReducers({
    isLogged
});
