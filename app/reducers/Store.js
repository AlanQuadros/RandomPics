import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from "redux-thunk";
import LoginReducer from './LoginReducer';
import PictureReducer from './PictureReducer';

const createMainStore = () => {
    const reducers = combineReducers({
        LoginReducer,
        PictureReducer
    });

    return createStore(reducers, applyMiddleware(thunk));
};

export default createMainStore;
