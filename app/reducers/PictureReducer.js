import { combineReducers } from 'redux';

const pictures = (state = false, action) => {
    switch (action.type) {
        case 'PICTURES_REQUEST':
            return action.pictures;
        case 'PICTURES_REQUEST_ERROR':
            return action.errorMessage;
        default:
            return state;
    }
};

const sendPicture = (state = false, action) => {
    switch (action.type) {
        case 'PICTURE_SEND':
            return true;
        case 'PICTURE_SEND_ERROR':
            return action.errorMessage;
        default:
            return state;
    }
};

export default combineReducers({
    pictures, sendPicture
});
